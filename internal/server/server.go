package server

import (
	"fmt"
	"net/http"

	"ideal/internal/middleware"
)

type Server struct {
	router             *http.ServeMux
	defaultMiddlewares []middleware.Middleware
}

func NewServer(middlewares ...middleware.Middleware) Server {
	return Server{
		router:             http.NewServeMux(),
		defaultMiddlewares: middlewares,
	}
}

func (s Server) Handle(route string, handler http.Handler, middlewares ...middleware.Middleware) {
	ms := append(s.defaultMiddlewares, middlewares...)

	for i := len(ms) - 1; i >= 0; i-- {
		handler = ms[i](handler)
	}

	s.router.Handle(route, handler)
}

func (s Server) Start(addr string) {
	fmt.Printf("starting at %s\n", addr)
	http.ListenAndServe(addr, s.router)
}
