package healthcheck

import (
	"fmt"
	"net/http"
)

type healthcheck struct{}

func (healthcheck) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Printf(">> healthcheck handler\n")
	fmt.Fprintf(w, "Healthcheck Response")
}

func Handler() http.Handler {
	return healthcheck{}
}
