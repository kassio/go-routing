package pages

import (
	"fmt"
	"net/http"
)

func Handler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf(">> pages handler\n")
		fmt.Fprintf(w, "PAGES Response")
	})
}
