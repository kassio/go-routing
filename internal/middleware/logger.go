package middleware

import (
	"fmt"
	"net/http"
)

func SomeMiddleware(prefix string) Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Printf("[MIDDLEWARE %s]\n", prefix)
			next.ServeHTTP(w, r)
		})
	}
}
