package main

import (
	"ideal/internal/healthcheck"
	"ideal/internal/middleware"
	"ideal/internal/pages"
	srv "ideal/internal/server"
)

func main() {
	server := srv.NewServer(
		middleware.SomeMiddleware("rejectmethods"),
		middleware.SomeMiddleware("urilimiter"),
		middleware.SomeMiddleware("correlation"),
		middleware.SomeMiddleware("metricsMiddleware"),
		middleware.SomeMiddleware("BasicAccessLogger"),
		middleware.SomeMiddleware("PanicMiddleware"),
		middleware.SomeMiddleware("customheaders"),
	)

	server.Handle("/-/healthcheck", healthcheck.Handler())
	server.Handle(
		"/",
		pages.Handler(),
		middleware.SomeMiddleware("RateLimiter"),
		middleware.SomeMiddleware("HTTPSRedirect"),
	)

	server.Start(":8080")
}
